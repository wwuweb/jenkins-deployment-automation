#!/bin/bash

#===============================================================================
#
# FILE: local_sync.sh
#
# USAGE: ./local_sync.sh <source_path> <destination_path>
#
# DESCRIPTION: Sync the source path to the destination path. The entire
#   directory trailing the source_path will be synced into the destination path.
#
# ARGUMENTS:
#   source_path: The local path of the project to deploy.
#   destination_path: Absolute path to the deployment location on the local
#     machine.
#
# AUTHOR:
#   WebTech 2017
#
#===============================================================================

set -o errexit
set -o notify
set -o nounset
set -o pipefail

readonly __DIR__="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

__main()
{
  if [ "$#" -ne 2 ]; then
    printf "Wrong number of arguments\n"
    printf "Usage: <source_path> <destination_path> \n"
    exit 1
  fi

  local source_path="$1"
  local destination_path="$2"

  local directory="$(basename "${source_path}")"

  printf "\nDeploying to ${destination_path}/${directory} ...\n\n"

  rsync \
    --verbose \
    --recursive \
    --delete \
    --delete-excluded \
    --prune-empty-dirs \
    --ignore-times \
    --filter="merge ${__DIR__}/filter.rules" \
    ${source_path} ${destination_path}
}

__main $@
